//
//  JayWayRobotTests.swift
//  JayWayRobotTests
//
//  Created by Paolo Longato on 10/05/2016.
//  Copyright © 2016 Paolo Longato. All rights reserved.
//

import XCTest
@testable import JayWayRobot

class JayWayRobotTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testCreateInvalidGrids() {
        var grids: [Grid?] = []
        grids.append(Grid(width: -1, height: 2))
        grids.append(Grid(width: 2, height: -1))
        grids.append(Grid(width: -2, height: -1))
        grids.append(Grid(width: 2, height: 0))
        grids.append(Grid(width: 0, height: 10))
        grids.append(Grid(width: 0, height: 0))
        grids.append(Grid(width: 0, height: -1))
        grids.append(Grid(width: -10, height: 0))
        let success = grids.reduce(true, combine: { (cum, new) in
            if new == nil {
                return cum && true
            } else {
                return false
            }
        })
        XCTAssert(success)
        _ = grids.map { print($0) }
    }
    
    func testCreateValidGrid() {
        let grid = Grid(width: 10, height: 100)
        XCTAssert(grid != nil)
        print(grid)
    }
    
    func testCreateInvalidPositions() {
        var pos: [Position?] = []
        pos.append(Position(column: -1, row: 2))
        pos.append(Position(column: 2, row: -2))
        pos.append(Position(column: -2, row: -1))
        pos.append(Position(column: 0, row: -1))
        pos.append(Position(column: -2, row: 0))
        let success = pos.reduce(true, combine: { (cum, new) in
            if new == nil {
                return cum && true
            } else {
                return false
            }
        })
        XCTAssert(success)
        _ = pos.map { print($0) }
    }
    
    func testCreateValidPositions() {
        var pos: [Position?] = []
        pos.append(Position(column: 0, row: 0))
        pos.append(Position(column: 2, row: 0))
        pos.append(Position(column: 0, row: 1))
        pos.append(Position(column: 2, row: 1))
        let success = pos.reduce(true, combine: { (cum, new) in
            if new == nil {
                return cum && false
            } else {
                return true
            }
        })
        XCTAssert(success)
        _ = pos.map { print($0) }
    }
    
    func testCreateInvalidRobots() {
        var robots: [Robot?] = []
        // Invalid grid => Taken care of by optional binding, no need to test
        // Invalid position => Taken care of by optional binding, no need to test
        // Valid grid, valid position, position outside grid
        let grid = Grid(width: 10, height: 10)!
        var initialPosition = Position(column: 11, row: 5)!
        robots.append(Robot(grid: grid, position: initialPosition, heading: .N))
        initialPosition = Position(column: 5, row: 11)!
        robots.append(Robot(grid: grid, position: initialPosition, heading: .N))
        initialPosition = Position(column: 11, row: 11)!
        robots.append(Robot(grid: grid, position: initialPosition, heading: .N))
        let success = robots.reduce(true, combine: { (cum, new) in
            if new == nil {
                return cum && true
            } else {
                return false
            }
        })
        XCTAssert(success)
        _ = robots.map { print($0 == nil) }
    }
    
    func testCreateValidRobot() {
        let grid = Grid(width: 10, height: 10)!
        let initialPosition = Position(column: 5, row: 5)!
        let robot = Robot(grid: grid, position: initialPosition, heading: .N)
        XCTAssert(robot != nil)
    }
    
    func testRightRotation() {
        let grid = Grid(width: 10, height: 10)!
        let initialPosition = Position(column: 5, row: 5)!
        let robot = Robot(grid: grid, position: initialPosition, heading: .N)
        let newHeading = robot?.rotateRight()
        // Is the new heading correct and coherent with internal state?
        XCTAssert(newHeading == .E)
        XCTAssert(robot?.heading == .E)
    }
    
    func testLeftRotation() {
        let grid = Grid(width: 10, height: 10)!
        let initialPosition = Position(column: 5, row: 5)!
        let robot = Robot(grid: grid, position: initialPosition, heading: .N)
        let newHeading = robot?.rotateLeft()
        // Is the new heading correct and coherent with internal state?
        XCTAssert(newHeading == .W)
        XCTAssert(robot?.heading == .W)
    }
    
    func testForwardMovementMiddle() {
        var robots: [Robot] = []
        let grid = Grid(width: 10, height: 10)!
        let initialPosition = Position(column: 5, row: 5)!
        robots.append(Robot(grid: grid, position: initialPosition, heading: .N)!)
        robots.append(Robot(grid: grid, position: initialPosition, heading: .E)!)
        robots.append(Robot(grid: grid, position: initialPosition, heading: .S)!)
        robots.append(Robot(grid: grid, position: initialPosition, heading: .W)!)
        let newPositions = robots.map { $0.stepForward() }
        // Internal state check
        _ = robots.enumerate().map { XCTAssert($0.element.position.isEqual(newPositions[$0.index]) ) }
        // Absolute state check
        XCTAssert(robots[0].position.isEqual(Position(column: 5, row: 4)!))
        XCTAssert(robots[1].position.isEqual(Position(column: 6, row: 5)!))
        XCTAssert(robots[2].position.isEqual(Position(column: 5, row: 6)!))
        XCTAssert(robots[3].position.isEqual(Position(column: 4, row: 5)!))
    }
    
    func testForwardMovementTopBorder() {
        var robots: [Robot] = []
        let grid = Grid(width: 10, height: 10)!
        let initialPosition = Position(column: 5, row: 0)!
        robots.append(Robot(grid: grid, position: initialPosition, heading: .N)!)
        robots.append(Robot(grid: grid, position: initialPosition, heading: .E)!)
        robots.append(Robot(grid: grid, position: initialPosition, heading: .S)!)
        robots.append(Robot(grid: grid, position: initialPosition, heading: .W)!)
        let newPositions = robots.map { $0.stepForward() }
        // Internal state check
        _ = robots.enumerate().map { XCTAssert($0.element.position.isEqual(newPositions[$0.index]) ) }
        // Absolute state check
        XCTAssert(robots[0].position.isEqual(Position(column: 5, row: 0)!))
        XCTAssert(robots[1].position.isEqual(Position(column: 6, row: 0)!))
        XCTAssert(robots[2].position.isEqual(Position(column: 5, row: 1)!))
        XCTAssert(robots[3].position.isEqual(Position(column: 4, row: 0)!))
    }
    
    func testForwardMovementLeftBorder() {
        var robots: [Robot] = []
        let grid = Grid(width: 10, height: 10)!
        let initialPosition = Position(column: 0, row: 5)!
        robots.append(Robot(grid: grid, position: initialPosition, heading: .N)!)
        robots.append(Robot(grid: grid, position: initialPosition, heading: .E)!)
        robots.append(Robot(grid: grid, position: initialPosition, heading: .S)!)
        robots.append(Robot(grid: grid, position: initialPosition, heading: .W)!)
        let newPositions = robots.map { $0.stepForward() }
        // Internal state check
        _ = robots.enumerate().map { XCTAssert($0.element.position.isEqual(newPositions[$0.index]) ) }
        // Absolute state check
        XCTAssert(robots[0].position.isEqual(Position(column: 0, row: 4)!))
        XCTAssert(robots[1].position.isEqual(Position(column: 1, row: 5)!))
        XCTAssert(robots[2].position.isEqual(Position(column: 0, row: 6)!))
        XCTAssert(robots[3].position.isEqual(Position(column: 0, row: 5)!))
    }
    
    func testForwardMovementRightBorder() {
        var robots: [Robot] = []
        let grid = Grid(width: 10, height: 10)!
        let initialPosition = Position(column: 9, row: 5)!
        robots.append(Robot(grid: grid, position: initialPosition, heading: .N)!)
        robots.append(Robot(grid: grid, position: initialPosition, heading: .E)!)
        robots.append(Robot(grid: grid, position: initialPosition, heading: .S)!)
        robots.append(Robot(grid: grid, position: initialPosition, heading: .W)!)
        let newPositions = robots.map { $0.stepForward() }
        // Internal state check
        _ = robots.enumerate().map { XCTAssert($0.element.position.isEqual(newPositions[$0.index]) ) }
        // Absolute state check
        XCTAssert(robots[0].position.isEqual(Position(column: 9, row: 4)!))
        XCTAssert(robots[1].position.isEqual(Position(column: 9, row: 5)!))
        XCTAssert(robots[2].position.isEqual(Position(column: 9, row: 6)!))
        XCTAssert(robots[3].position.isEqual(Position(column: 8, row: 5)!))
    }
    
    func testForwardMovementBottomBorder() {
        var robots: [Robot] = []
        let grid = Grid(width: 10, height: 10)!
        let initialPosition = Position(column: 5, row: 9)!
        robots.append(Robot(grid: grid, position: initialPosition, heading: .N)!)
        robots.append(Robot(grid: grid, position: initialPosition, heading: .E)!)
        robots.append(Robot(grid: grid, position: initialPosition, heading: .S)!)
        robots.append(Robot(grid: grid, position: initialPosition, heading: .W)!)
        let newPositions = robots.map { $0.stepForward() }
        // Internal state check
        _ = robots.enumerate().map { XCTAssert($0.element.position.isEqual(newPositions[$0.index]) ) }
        // Absolute state check
        XCTAssert(robots[0].position.isEqual(Position(column: 5, row: 8)!))
        XCTAssert(robots[1].position.isEqual(Position(column: 6, row: 9)!))
        XCTAssert(robots[2].position.isEqual(Position(column: 5, row: 9)!))
        XCTAssert(robots[3].position.isEqual(Position(column: 4, row: 9)!))
    }
    
    func testForwardMovementNECorner() {
        var robots: [Robot] = []
        let grid = Grid(width: 10, height: 10)!
        let initialPosition = Position(column: 9, row: 0)!
        robots.append(Robot(grid: grid, position: initialPosition, heading: .N)!)
        robots.append(Robot(grid: grid, position: initialPosition, heading: .E)!)
        robots.append(Robot(grid: grid, position: initialPosition, heading: .S)!)
        robots.append(Robot(grid: grid, position: initialPosition, heading: .W)!)
        let newPositions = robots.map { $0.stepForward() }
        // Internal state check
        _ = robots.enumerate().map { XCTAssert($0.element.position.isEqual(newPositions[$0.index]) ) }
        // Absolute state check
        XCTAssert(robots[0].position.isEqual(Position(column: 9, row: 0)!))
        XCTAssert(robots[1].position.isEqual(Position(column: 9, row: 0)!))
        XCTAssert(robots[2].position.isEqual(Position(column: 9, row: 1)!))
        XCTAssert(robots[3].position.isEqual(Position(column: 8, row: 0)!))
    }
    
    func testForwardMovementNWCorner() {
        var robots: [Robot] = []
        let grid = Grid(width: 10, height: 10)!
        let initialPosition = Position(column: 0, row: 0)!
        robots.append(Robot(grid: grid, position: initialPosition, heading: .N)!)
        robots.append(Robot(grid: grid, position: initialPosition, heading: .E)!)
        robots.append(Robot(grid: grid, position: initialPosition, heading: .S)!)
        robots.append(Robot(grid: grid, position: initialPosition, heading: .W)!)
        let newPositions = robots.map { $0.stepForward() }
        // Internal state check
        _ = robots.enumerate().map { XCTAssert($0.element.position.isEqual(newPositions[$0.index]) ) }
        // Absolute state check
        XCTAssert(robots[0].position.isEqual(Position(column: 0, row: 0)!))
        XCTAssert(robots[1].position.isEqual(Position(column: 1, row: 0)!))
        XCTAssert(robots[2].position.isEqual(Position(column: 0, row: 1)!))
        XCTAssert(robots[3].position.isEqual(Position(column: 0, row: 0)!))
    }
    
    func testForwardMovementSECorner() {
        var robots: [Robot] = []
        let grid = Grid(width: 10, height: 10)!
        let initialPosition = Position(column: 9, row: 9)!
        robots.append(Robot(grid: grid, position: initialPosition, heading: .N)!)
        robots.append(Robot(grid: grid, position: initialPosition, heading: .E)!)
        robots.append(Robot(grid: grid, position: initialPosition, heading: .S)!)
        robots.append(Robot(grid: grid, position: initialPosition, heading: .W)!)
        let newPositions = robots.map { $0.stepForward() }
        // Internal state check
        _ = robots.enumerate().map { XCTAssert($0.element.position.isEqual(newPositions[$0.index]) ) }
        // Absolute state check
        XCTAssert(robots[0].position.isEqual(Position(column: 9, row: 8)!))
        XCTAssert(robots[1].position.isEqual(Position(column: 9, row: 9)!))
        XCTAssert(robots[2].position.isEqual(Position(column: 9, row: 9)!))
        XCTAssert(robots[3].position.isEqual(Position(column: 8, row: 9)!))
    }
    
    func testForwardMovementSWCorner() {
        var robots: [Robot] = []
        let grid = Grid(width: 10, height: 10)!
        let initialPosition = Position(column: 0, row: 9)!
        robots.append(Robot(grid: grid, position: initialPosition, heading: .N)!)
        robots.append(Robot(grid: grid, position: initialPosition, heading: .E)!)
        robots.append(Robot(grid: grid, position: initialPosition, heading: .S)!)
        robots.append(Robot(grid: grid, position: initialPosition, heading: .W)!)
        let newPositions = robots.map { $0.stepForward() }
        // Internal state check
        _ = robots.enumerate().map { XCTAssert($0.element.position.isEqual(newPositions[$0.index]) ) }
        // Absolute state check
        XCTAssert(robots[0].position.isEqual(Position(column: 0, row: 8)!))
        XCTAssert(robots[1].position.isEqual(Position(column: 1, row: 9)!))
        XCTAssert(robots[2].position.isEqual(Position(column: 0, row: 9)!))
        XCTAssert(robots[3].position.isEqual(Position(column: 0, row: 9)!))
    }
    
    func testEmptyCommandString() {
        let grid = Grid(width: 5, height: 5)!
        let pos = Position(column: 2, row: 2)!
        let robot = Robot(grid: grid, position: pos, heading: .S)!
        let string = ""
        do {
            try robot.executeCommandSequence(string: string)
        } catch CommandStringError.EmptyString {
            XCTAssert(true)
        } catch  {
            XCTAssert(false)
        }
    }
    
    func testInvalidCommandsInString() {
        let grid = Grid(width: 5, height: 5)!
        let pos = Position(column: 2, row: 2)!
        let robot = Robot(grid: grid, position: pos, heading: .S)!
        let string = "LRFFFa9FFFaLgty9999"
        do {
            try robot.executeCommandSequence(string: string)
        } catch CommandStringError.InvalidCommand {
            XCTAssert(true)
        } catch  {
            XCTAssert(false)
        }
    }
    
    func testValidCommandStringA() {
        let grid = Grid(width: 5, height: 5)!
        let pos = Position(column: 1, row: 2)!
        let robot = Robot(grid: grid, position: pos, heading: .N)!
        let string = "RF RF Fr frf"
        do {
            let output: (position: Position, heading: Heading) = try robot.executeCommandSequence(string: string)
            XCTAssert(output.position.isEqual(Position(column: 1, row: 3)!) && output.heading == .N)
        } catch {
            XCTAssert(false)
        }
        
    }

    func testValidCommandStringB() {
        let grid = Grid(width: 5, height: 5)!
        let pos = Position(column: 0, row: 0)!
        let robot = Robot(grid: grid, position: pos, heading: .E)!
        let string = "RFL \n FFLrF"
        do {
            let output: (position: Position, heading: Heading) = try robot.executeCommandSequence(string: string)
            XCTAssert(output.position.isEqual(Position(column: 3, row: 1)!) && output.heading == .E)
        } catch {
            XCTAssert(false)
        }
        
    }
    
    func testValidCommandStringHittingBoundary() {
        let grid = Grid(width: 5, height: 5)!
        let pos = Position(column: 0, row: 0)!
        let robot = Robot(grid: grid, position: pos, heading: .E)!
        let string = "FFFFFFFFFFRFFFFFFFFFFFFFRFFFFFFFFFFFFFFFFFRFFFFFFFFFFFFFFFRRRRLLL"
        do {
            let output: (position: Position, heading: Heading) = try robot.executeCommandSequence(string: string)
            XCTAssert(output.position.isEqual(Position(column: 0, row: 0)!) && output.heading == .E)
        } catch {
            XCTAssert(false)
        }
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measureBlock {
            // Put the code you want to measure the time of here.
        }
    }
    
}
