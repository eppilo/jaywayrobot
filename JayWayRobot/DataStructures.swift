//
//  DataStructures.swift
//  JayWayRobot
//
//  Created by Paolo Longato on 10/05/2016.
//  Copyright © 2016 Paolo Longato. All rights reserved.
//

import Foundation

// Grid made of columns and rows
struct Grid {
    let width: Int
    let height: Int
    init?(width: Int, height: Int) {
        guard width > 0 && height > 0 else {
            return nil
        }
        self.height = height
        self.width = width
    }
}

// Row and column position on a grid
struct Position {
    let column: Int
    let row: Int
    init?(column: Int, row: Int) {
        guard column > -1 && row > -1 else {
            return nil
        }
        self.column = column
        self.row = row
    }
    func isEqual(other: Position) -> Bool {
        return self.column == other.column && self.row == other.row
    }
}

// Heading for the robot. Rotation logic is self contained and therefore incapsulated
enum Heading: Int {
    case N = 0
    case E = 1
    case S = 2
    case W = 3

    var description: String {
        get {
            switch self {
            case .N: return "N"
            case .S: return "S"
            case .W: return "W"
            case .E: return "E"
            }
        }
    }
    
    func rotateRight() -> Heading {
        let newHeadingRawValue = self.rawValue + 1
        if newHeadingRawValue > 3 {
            return .N
        }
        return Heading(rawValue: newHeadingRawValue)!
    }
    
    func rotateLeft() -> Heading {
        let newHeadingRawValue = self.rawValue - 1
        if newHeadingRawValue < 0 {
            return .W
        }
        return Heading(rawValue: newHeadingRawValue)!
    }

}

// For command string
enum CommandStringError: ErrorType {
    case InvalidCommand
    case EmptyString
}
