//
//  Robot.swift
//  JayWayRobot
//
//  Created by Paolo Longato on 10/05/2016.
//  Copyright © 2016 Paolo Longato. All rights reserved.
//

import Foundation

final class Robot {
    var grid: Grid
    var position: Position
    var heading: Heading
    
    // Designated initializer.
    // A robot sould always have a grid a position and a heading.
    // A robot should always be created in a valid position wrt its grid
    init?(grid: Grid, position: Position, heading: Heading) {
        self.grid = grid
        self.position = position
        self.heading = heading
        guard position.column < grid.width && position.row < grid.height else {
            return nil
        }
    }
    
    // Left rotaion logic is incapsulated the Heading enum
    // Return value is a copy of the new heading
    func rotateLeft() -> Heading {
        let newHeading = heading.rotateLeft()
        heading = newHeading
        return newHeading
    }
    
    // Right rotaion logic is incapsulated the Heading enum
    // Return value is a copy of the new heading
    func rotateRight() -> Heading {
        let newHeading = heading.rotateRight()
        heading = newHeading
        return newHeading
    }
    
    // Step forrward logic takes care of corner cases:
    // robot is on one or two borders => robot does not move towards the relevant border(s)
    // Return value is a copy of the new position
    func stepForward() -> Position {
        var newColumn = self.position.column
        var newRow = self.position.row
        switch self.heading {
        case .N:
            if position.row > 0 {
                newRow -= 1
            } else {
                print("Attempting to breach North boundary")
            }
        case .S:
            if position.row < grid.height - 1 {
                newRow += 1
            } else {
                print("Attempting to breach South boundary")
            }
        case .E:
            if position.column < grid.width - 1 {
                newColumn += 1
            } else {
                print("Attempting to breach East boundary")
            }
        case .W:
            if position.column > 0 {
                newColumn -= 1
            } else {
                print("Attempting to breach West boundary")
            }
        }
        let newPosition = Position(column: newColumn, row: newRow)!
        self.position = newPosition
        return newPosition
    }
    
    // Parse and execute a string of commands: L, R, F
    func executeCommandSequence(string string: String) throws -> (Position, Heading) {
        
        // Make a string uppper case and trim the white spaces and new lines.  This is a design choice: immagine a situation where user might want to format the input string by using white spaces and new lines
        let cs = string
            .stringByReplacingOccurrencesOfString(" ", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
            .stringByReplacingOccurrencesOfString("\n", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
            .stringByReplacingOccurrencesOfString("\t", withString: "", options: NSStringCompareOptions.LiteralSearch, range: nil)
            .uppercaseString
        
        // Empty command string throws an error
        if cs.isEmpty {
            throw CommandStringError.EmptyString
        }
        
        // Check validity of the string before executing ANY command. This is a design choice: immagine the case when an invalid string has some valid commands by chance.
        for char in cs.characters {
            if char != "L" && char != "F" && char != "R" {
                throw CommandStringError.InvalidCommand
            }
        }
        
        // Execute all the commands
        for char in cs.characters {
            switch char {
                case "L":
                    self.rotateLeft()
                case "R":
                    self.rotateRight()
                case "F":
                    self.stepForward()
                default: break
            }
        }
        
        // Return copy of the (new) current position
        return (Position(column: self.position.column, row: self.position.row)!, Heading(rawValue: self.heading.rawValue)!)
        
    }
    
}