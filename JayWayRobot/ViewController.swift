//
//  ViewController.swift
//  JayWayRobot
//
//  Created by Paolo Longato on 10/05/2016.
//  Copyright © 2016 Paolo Longato. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextViewDelegate {

    var robot: Robot?
    
    private let shiftConstant: CGFloat = 205
    private let animationTime: NSTimeInterval = 0.42
    @IBOutlet weak var commandBox: UITextView!
    @IBOutlet weak var topConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var stateSwitch: UISwitch!
    
    // Steppers
    @IBOutlet weak var wStepper: UIStepper!
    @IBOutlet weak var hStepper: UIStepper!
    @IBOutlet weak var colStepper: UIStepper!
    @IBOutlet weak var rowStepper: UIStepper!
    @IBOutlet weak var headingStepper: UIStepper!
    
    // Labels
    @IBOutlet weak var outputLabel: UILabel!
    @IBOutlet var stepperLabels: [UILabel]!
    
    // MARK: Initialisation and memory management
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Initialise default Robot
        self.makeRobotFromSteppers(clearOutputLine: true)
        // Delegates
        self.commandBox.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: TextView delegate methods
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            self.view.layoutIfNeeded()
            self.bottomConstraint.constant = 0
            self.topConstraint.constant = 0
            UIView.animateWithDuration(self.animationTime, animations: {
                self.view.layoutIfNeeded()
            })
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    func textViewDidBeginEditing(textView: UITextView) {
        self.view.layoutIfNeeded()
        self.bottomConstraint.constant = self.shiftConstant
        self.topConstraint.constant = -self.shiftConstant
        // Careful: strong reference to self (not a problem here as self is never deallocated in this app)
        UIView.animateWithDuration(self.animationTime, animations: {
            self.view.layoutIfNeeded()
        })
        
    }
    
    // MARK: Stepper actions
    
    @IBAction func steppersHaveUpdated(sender: UIStepper) {
        makeRobotFromSteppers(clearOutputLine: true)
        _ = self.stepperLabels.map {
            if $0.tag == sender.tag {
                if sender.tag < 4 {
                    $0.text = "\(Int(sender.value))"
                } else if sender.tag == 4 {
                    $0.text = Heading(rawValue: Int(sender.value))?.description
                }
            }
        }
    }
    
    // MARK: Execute command action
    
    @IBAction func executeCommandButtonPress(sender: UIButton) {
        guard robot != nil else { return }
        do {
            let output: (position: Position, heading: Heading) = try self.robot!.executeCommandSequence(string: self.commandBox.text)
            self.outputLabel.text = "POS = \(output.position.column),\(output.position.row) HEADING = \(output.heading.description)"
            if !stateSwitch.on {
                makeRobotFromSteppers(clearOutputLine: false)
            } else {
                // Update steppers
                colStepper.value = Double(output.position.column)
                rowStepper.value = Double(output.position.row)
                headingStepper.value = Double(output.heading.rawValue)
                // Update labels
                _ = stepperLabels.map {
                    if $0.tag == 2 {
                        $0.text = "\(Int(colStepper.value))"
                    } else if $0.tag == 3 {
                        $0.text = "\(Int(rowStepper.value))"
                    } else if $0.tag == 4 {
                         $0.text = Heading(rawValue: Int(headingStepper.value))?.description
                    }
                }
            }
        } catch CommandStringError.EmptyString {
            outputLabel.text = "Input string is empty"
        } catch CommandStringError.InvalidCommand {
            outputLabel.text = "Invalid command(s) in string"
        } catch {
            outputLabel.text = "Generic error"
        }
    }
    
    
    // MARK: Helper functions
    
    func makeRobotFromSteppers(clearOutputLine clear: Bool) {
        let grid = Grid(width: Int(wStepper.value), height: Int(hStepper.value))
        let pos = Position(column: Int(colStepper.value), row: Int(rowStepper.value))
        guard grid != nil && pos != nil else { self.robot = nil; return }
        self.robot = Robot(grid: grid!, position: pos!, heading: Heading(rawValue: Int(headingStepper.value))!)
        guard clear else { return }
        if robot == nil {
            outputLabel.text = "Invalid initial position."
        } else {
            outputLabel.text = "Robot ready."
        }
    }
    
}

